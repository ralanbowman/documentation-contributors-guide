= Write contributions to Docs
Fedora Documentation Team <https://discussion.fedoraproject.org/tag/docs>
:revnumber: F36 and newer
:revdate: 2023-02-07

:page-aliases: contributing

[abstract]
____
This document explains how to work with the publishing system used to build the Fedora Documentation website. It will guide you through contributing to existing documentation as well as creating completely new content sets and publishing them in the English originals as well as any possible translations.
____

There are many different ways to contribute to Fedora documentation. Some of them are designed to enable contributions without technical knowledge about Web Content Management Systems and about how to store and manage your contribution. The tool takes over all these issues for you. It enables authors to concentrate on the topic at hand.

Please check us out. If you come across a documentation page that contains an error or inaccuracy, use one of the casual contribution tools described below to improve the page. You can't break anything, only improve it! If you have any questions, please do not hesitate to contact the Docs team.

Other tools are designed to provide a powerful working environment for accomplished authors. They enable efficient work even on large complex interrelated text collections. These tools are aimed at experienced authors. 

== How it works

Fedora documentation uses the Antora WEB CMS to build and manage the Web site. Documentation is once created static content, with updates from time to time. This is exactly what Antora specializes in. It gathers static text documents and transforms them into a complete Web site, including navigation, links, formatting, positioning, adaptation to different output devices, etc. For more general information about the Antora publishing system, see the https://antora.org/[Antora website] and https://docs.antora.org/antora/latest/page/[Antora documentation]. 

As a writer, you can fully concentrate on the content and your message and write away.

=== The general procedure

The 4-eyes principle applies to the Fedora documentation. A different author reviews each contribution. Technically, when you complete your text or text modification, the system creates a "Pull Request" or "Merge Request" to integrate your text into the documentation body. This triggers other authors, specifically board members or members specifically committed to the part of the documentation body in question, to start a review and either initiates an inclusion or starts a discussion. Allow 2-3 days for an answer to a request.

=== Some technical background

Fedora uses _AsciiDoc_ to format text in a simple and efficient way. It closely follows natural writing styles in everyday notes for structuring and highlighting. In this way, you can use any editor, including almost any word processor that can edit and save Ascii Text.  More about this below.

The AsciiDoc text document is stored in series of GIT repositories. This is a system especially popular among software developers, but also very capable for managing text documents. GIT encourages and facilitates the use of the 4-eyes principle by a "Pull (or Merge) Request Workflow". You only need to worry about the details of this workflow if you want to set up a local work environment intended for professional and frequent contributions. All other tools take care of the necessary steps in the background. 


== Prerequisites

The only requirements for contributing documentation to Fedora Docs are:

* link:++https://admin.fedoraproject.org/accounts/++[*Fedora Account System*] (*FAS*) account.
* GitLab account
* Must have signed https://fedoraproject.org/wiki/Legal:Fedora_Project_Contributor_Agreement[Fedora Project Contributor Agreement] from FAS (see https://admin.fedoraproject.org/accounts/group/view/cla_done[here])
* Basic knowledge of *AsciiDoc* markup language (see xref:contributing-docs/asciidoc-markup.adoc[AsciiDoc for Fedora])


== The tools

The quick way – Edit a specific file::
   Edit a specific file you want to improve.
   Suitable for minor fixes of existing pages
   
The easy way – Use the GitLab web IDE::
   Quick and easy so there is no need to install anything in local computer. 
   Most of the steps are automatic. Suitable for editing existing articles as well as new ones.
   
The advanced way –  Create a local writing environment::
   Work with multiple files and repos offline at your pace using your choice of text editor and terminal.
   You can build and render the pages locally to test your changes using Podman container.

== Typical ways to contribute

One can distinguish several typical types of contribution to Fedora documentation, for which the available tools are suitable in different ways.

Many ways for various types of contribution, e.g., typo fixes, adding short information or a link, update an article, contribute a new article.

=== Update an existing documentation page 

This typically involves minor changes, e.g., typo fixes, adding short information or a link, or a correction to the text. This type is especially necessary whenever the documentation needs to be updated due to a new software version. 

The _File Edit tools_ are convenient for this purpose. 

This is specifically true for _casual contributions_ when a user detects outdated or inaccurate information.

=== Extend an existing documentation domain
   
This typically involves adding one of more new chapter or section(s) with several chapters, e.g., adding another container technique to the documentation of containerization. 

The _Web interface_ is a convenient tool for this purpose. A local writing environment is useable as well, but may involve too much overhead, if you want contribute to just one documentation.

It is highly recommended to present the plan on one of the Docs communication channels before starting. You may get suggestions and tips on content, but also on editing, e.g., file structure and naming conventions.

=== Introduce a new documentation area

This typically involves a new extensive subject area, such as new software or a new administration tool. Typically, it includes several sections and chapters and requires the creation of a new, separate repository.

For this type, setting up a local writing environment is useful and worthwhile. However, the web interface would also be usable.

In any case, a prior discussion with the Docs team is necessary, if only to create the technical prerequisites.
